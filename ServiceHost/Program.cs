﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace AutoPhoto.ServiceHost {
    class Program {
        static void Main(string[] args) {
            var watch = ConfigurationManager.AppSettings["WatchFolder"];
            var drops = ConfigurationManager.AppSettings["DropFolders"].Split(';');
            var passport = ConfigurationManager.AppSettings["PassportFolder"];

            HostFactory.Run(x =>
            {
                x.Service<PhotoService>(s =>
                {
                    s.ConstructUsing(name => new PhotoService(watch, drops));
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("AutoPhoto Organizer");
                x.SetDisplayName("AutoPhoto");
                x.SetServiceName("autophoto");
            });
        }
    }
}
