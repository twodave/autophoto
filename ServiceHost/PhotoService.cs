﻿using Polly;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace AutoPhoto.ServiceHost {
    public class PhotoService {
        private readonly string watch;
        private readonly string[] drops;

        private readonly Policy retryPolicy;

        private const string PHOTO_DEST_TEMPLATE = @"Pictures\{0}\{1}\{2}";
        private const string VIDEO_DEST_TEMPLATE = @"Videos\{0}\{1}\{2}";

        private bool stopping = false;

        public PhotoService(string watch, string[] drop) {
            this.watch = watch;
            this.drops = drop;

            retryPolicy = Policy.Handle<Exception>().Retry(3, (ex, num) => { Thread.Sleep(500); });
        }

        public void Start() {
            new Thread(LoopingThread).Start();
        }

        public void Stop() {
            stopping = true;
        }
        
        private void LoopingThread() {
            while (!stopping) {
                try {
                    var watchDrive = watch.Substring(0, watch.IndexOf('\\') + 1);
                    var dropDrives = drops.Select(d=>d.Substring(0, d.IndexOf('\\') + 1));

                    var drives = DriveInfo.GetDrives();

                    if (drives.Any(d => d.Name.Equals(watchDrive, StringComparison.OrdinalIgnoreCase) && d.IsReady)
                        && dropDrives.All(dropDrive=>drives.Any(d => d.Name.Equals(dropDrive, StringComparison.OrdinalIgnoreCase) && d.IsReady))) {
                        
                        Action<string, Func<string, string[]>> organize = (template, pathGetter) => {

                            foreach (var media in pathGetter(watch)) {
                                DateTime mediaDate;
                                try {
                                    // most reliable datge
                                    mediaDate = GetDateTaken(media);
                                } catch {
                                    // there's no date taken property I suppose
                                    // let's just use last modified time as a fall-back
                                    mediaDate = File.GetLastWriteTime(media);
                                }

                                // form our desired destination path
                                foreach (var drop in drops) {
                                    var destination = Path.Combine(drop, String.Format(template, mediaDate.Year, GetMonthFolder(mediaDate.Month), Path.GetFileName(media)));

                                    // make sure the path is unused and find an unused path if it isn't
                                    var candidate = destination;
                                    int fails = 0;

                                    while (File.Exists(candidate)) {
                                        fails += 1;
                                        var dir = Path.GetDirectoryName(destination);
                                        var name = Path.GetFileNameWithoutExtension(destination);
                                        var ext = Path.GetExtension(destination);
                                        candidate = Path.Combine(dir, String.Format("{0} ({1}).{2}", name, fails, ext));
                                    }

                                    // final destination muahaha
                                    destination = candidate;

                                    // give it a few tries before we give up...

                                    try {
                                        retryPolicy.Execute(() => {
                                            Directory.CreateDirectory(Path.GetDirectoryName(destination));
                                            File.Copy(media, destination);
                                        });
                                    } catch {
                                        // let's move on, just leave it alone
                                    }
                                }
                                try {
                                    retryPolicy.Execute(() => {
                                        File.Delete(media);
                                    });
                                } catch {
                                    // let's move on, just leave it alone
                                }
                            }
                        };

                        organize(PHOTO_DEST_TEMPLATE, GetPhotoPaths);
                        organize(VIDEO_DEST_TEMPLATE, GetVideoPaths);

                        Thread.Sleep(15 * 60 * 1000); // sleep a few minutes
                    } else {
                        Thread.Sleep(15 * 1000); // sleep a few seconds
                    }
                }catch(Exception ex) {
                    ex.ToString();
                }
            }
        }

        private object GetMonthFolder(int month) {
            switch (month) {
                case 1:
                    return "01 - January";
                case 2:
                    return "02 - February";
                case 3:
                    return "03 - March";
                case 4:
                    return "04 - April";
                case 5:
                    return "05 - May";
                case 6:
                    return "06 - June";
                case 7:
                    return "07 - July";
                case 8:
                    return "08 - August";
                case 9:
                    return "09 - September";
                case 10:
                    return "10 - October";
                case 11:
                    return "11 - November";
                case 12:
                    return "12 - December";
                default:
                    return "00 - Unknown";
            }
        }

        private string[] GetPhotoPaths(string root) {
            var photos = Directory.GetFiles(root, "*.jpg", SearchOption.AllDirectories).AsEnumerable();
            photos = photos.Union(Directory.GetFiles(root, "*.JPG", SearchOption.AllDirectories));

            photos = photos.Union(Directory.GetFiles(root, "*.jpeg", SearchOption.AllDirectories));
            photos = photos.Union(Directory.GetFiles(root, "*.JPEG", SearchOption.AllDirectories));

            photos = photos.Union(Directory.GetFiles(root, "*.gif", SearchOption.AllDirectories));
            photos = photos.Union(Directory.GetFiles(root, "*.GIF", SearchOption.AllDirectories));

            photos = photos.Union(Directory.GetFiles(root, "*.png", SearchOption.AllDirectories));
            photos = photos.Union(Directory.GetFiles(root, "*.PNG", SearchOption.AllDirectories));

            photos = photos.Union(Directory.GetFiles(root, "*.bmp", SearchOption.AllDirectories));
            photos = photos.Union(Directory.GetFiles(root, "*.BMP", SearchOption.AllDirectories));

            return photos.ToArray();
        }

        private static string[] GetVideoPaths(string root) {
            var videos = Directory.GetFiles(root, "*.MTS", SearchOption.AllDirectories).AsEnumerable();
            videos = videos.Union(Directory.GetFiles(root, "*.mts", SearchOption.AllDirectories));

            videos = videos.Union(Directory.GetFiles(root, "*.MP4", SearchOption.AllDirectories));
            videos = videos.Union(Directory.GetFiles(root, "*.mp4", SearchOption.AllDirectories));

            videos = videos.Union(Directory.GetFiles(root, "*.AVI", SearchOption.AllDirectories));
            videos = videos.Union(Directory.GetFiles(root, "*.avi", SearchOption.AllDirectories));

            videos = videos.Union(Directory.GetFiles(root, "*.M4A", SearchOption.AllDirectories));
            videos = videos.Union(Directory.GetFiles(root, "*.m4a", SearchOption.AllDirectories));

            videos = videos.Union(Directory.GetFiles(root, "*.MOV", SearchOption.AllDirectories));
            videos = videos.Union(Directory.GetFiles(root, "*.mov", SearchOption.AllDirectories));

            return videos.ToArray();
        }

        //we init this once so that if the function is repeatedly called
        //it isn't stressing the garbage man
        private Regex r = new Regex(":");

        /// <summary>
        /// Retrieves the datetime WITHOUT loading the whole image
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private DateTime GetDateTaken(string path) {
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            using (Image myImage = Image.FromStream(fs, false, false)) {
                PropertyItem propItem = myImage.GetPropertyItem(36867);
                string dateTaken = r.Replace(Encoding.UTF8.GetString(propItem.Value), "-", 2);
                return DateTime.Parse(dateTaken);
            }
        }
    }
}
